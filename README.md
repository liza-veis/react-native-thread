# React Native Thread

## Downloading

1. Clone the repo

```
git clone https://liza-veis@bitbucket.org/liza-veis/react-native-thread.git
```

## Running client

1. To make requests you need to deploy a backend.

   For client side testing, you can just use already deployed server - https://react-native-thread-server.herokuapp.com/

   So, you won't need to configure and run the backend

2. Go to the client folder

3. Create a .env file following .env.example

4. Install all dependencies

5. Run `npx react-native start`

6. Open second terminal and in the client folder run `npx react-native run-android`

## Running backend

1. Go to the server folder

2. Create a .env file following .env.example

3. Install all dependencies

4. Run migrations

```
npx sequelize-cli db:migrate
npx sequelize-cli db:seed:all
```

5. Run `npm start` or `npm run dev`

import {showMessage} from 'react-native-flash-message';

export const showErrorMessage = (message: string = 'Something went wrong!') =>
  showMessage({
    type: 'danger',
    message,
  });

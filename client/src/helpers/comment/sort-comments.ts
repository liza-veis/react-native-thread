import dayjs from 'dayjs';

export const sortComments = (comments: CommentData[]) =>
  comments.slice().sort((a, b) => dayjs(b.createdAt).diff(a.createdAt));

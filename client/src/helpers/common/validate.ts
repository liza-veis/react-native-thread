import validator from 'validator';

import {InputType} from '@common';

export const validate = (value: string, type: InputType) => {
  switch (type) {
    case InputType.EMAIL: {
      return validator.isEmail(value);
    }

    default: {
      return !validator.isEmpty(value);
    }
  }
};

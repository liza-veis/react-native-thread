export const mapPostLikes = <T extends PostData | OpenedPostData>(
  post: T,
  diff: number,
) => ({
  ...post,
  likeCount: (Number(post.likeCount) + diff).toString(),
});

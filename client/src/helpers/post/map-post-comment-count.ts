export const mapPostCommentCount = (post: PostData | OpenedPostData) => ({
  ...post,
  commentCount: (Number(post.commentCount) + 1).toString(),
});

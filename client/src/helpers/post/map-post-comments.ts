import {mapPostCommentCount} from './map-post-comment-count';

export const mapPostComments = (
  post: OpenedPostData,
  comment: CommentData,
) => ({
  ...mapPostCommentCount(post),
  comments: post.comments.concat(comment),
});

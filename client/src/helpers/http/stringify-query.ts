import {stringify} from 'query-string';

export const stringifyQuery = (query: Query) => stringify(query);

import DocumentPicker from 'react-native-document-picker';

export const pickImage = async () =>
  DocumentPicker.pick({
    type: [DocumentPicker.types.images],
  });

import React, {FC} from 'react';
import {FormControl, Heading, VStack} from 'native-base';

import {Button} from '@components';
import {Colors, FontSize} from '@styles';

type AuthFormProps = {
  heading: string;
  buttonName: string;
  isLoading?: boolean;
  onSubmit: () => void;
};

export const AuthForm: FC<AuthFormProps> = ({
  heading,
  buttonName,
  onSubmit,
  isLoading,
  children,
}) => (
  <FormControl pr={8} pl={8} isReadOnly={isLoading}>
    <Heading
      fontSize={FontSize.XL}
      color={Colors.TEAl}
      textAlign="center"
      mt={5}>
      {heading}
    </Heading>
    <VStack w="100%" space={4} mt={5}>
      {children}
      <Button isLoading={isLoading} onPress={onSubmit}>
        {buttonName}
      </Button>
    </VStack>
  </FormControl>
);

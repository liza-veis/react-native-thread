import React, {FC} from 'react';
import {Box, Spinner} from 'native-base';

import {Colors} from '@styles';

type LoaderProps = {
  fullHeight?: boolean;
};

export const Loader: FC<LoaderProps> = ({fullHeight}) => (
  <Box
    alignSelf="center"
    justifyContent="center"
    height={fullHeight ? '100%' : 'auto'}>
    <Spinner color={Colors.TEAl_DARK} mt={5} />
  </Box>
);

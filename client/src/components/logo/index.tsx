import React, {FC} from 'react';
import {Flex, Heading, Image} from 'native-base';

import {LOGO_URL} from '@common';
import {Colors, FontSize} from '@styles';

export const Logo: FC = () => (
  <Flex flexDirection="row" alignItems="center">
    <Image source={{uri: LOGO_URL}} alt="Logo" size="md" mr={4} />
    <Heading fontSize={FontSize.XL} color={Colors.GRAY}>
      Thread
    </Heading>
  </Flex>
);

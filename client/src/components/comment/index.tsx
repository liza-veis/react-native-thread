import React, {FC} from 'react';
import {Image, Box, HStack, Text, VStack} from 'native-base';

import {getTimeFromNow} from '@helpers';
import {DEFAULT_USER_AVATAR} from '@common';
import {Colors, FontSize} from '@styles';

type CommentProps = {
  comment: CommentData;
};

export const Comment: FC<CommentProps> = ({comment}) => {
  const {body, createdAt, user} = comment;

  const time = getTimeFromNow(createdAt);

  return (
    <Box pr={9}>
      <HStack>
        <Image
          size={10}
          source={{uri: user.image?.link ?? DEFAULT_USER_AVATAR}}
          alt="User avatar"
          rounded={20}
        />
        <VStack ml={2} pt={1} space={1}>
          <HStack space={1}>
            <Text
              color={Colors.TEAl_DARK}
              fontSize={FontSize.SM}
              fontWeight="bold"
              lineHeight={4}>
              {user.username}
            </Text>
            <Text color={Colors.GRAY} fontSize={FontSize.SM} lineHeight={4}>
              {time}
            </Text>
          </HStack>
          <Text fontSize={FontSize.SM}>{body}</Text>
        </VStack>
      </HStack>
    </Box>
  );
};

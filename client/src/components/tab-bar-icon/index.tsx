import React, {FC} from 'react';
import {Icon} from '@components';
import {IconName} from '@common';
import {Colors} from '@styles';

type TabBarIconProps = {
  focused: boolean;
  iconName: IconName;
};

export const TabBarIcon: FC<TabBarIconProps> = ({focused, iconName}) => (
  <Icon name={iconName} color={focused ? Colors.TEAl : Colors.GRAY} />
);

import React, {FC} from 'react';
import {Icon as UIIcon, IIconProps, Text, HStack} from 'native-base';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome';

import {Colors} from '@styles';
import {IconName, IconSize} from '@common';

type IconProps = {
  name: IconName;
  label?: string;
  size?: IconSize;
  color?: Colors;
  props?: IIconProps;
};

export const Icon: FC<IconProps> = ({
  name,
  label,
  size = IconSize.SM,
  color = Colors.GRAY,
  props,
}) => (
  <HStack space={1} alignItems="center">
    <UIIcon
      as={<FontAwesomeIcons name={name} />}
      size={size}
      color={color}
      {...props}
    />
    {label && (
      <Text color={color} fontSize={size} lineHeight={4}>
        {label}
      </Text>
    )}
  </HStack>
);

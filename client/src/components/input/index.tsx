import React, {FC} from 'react';
import {Button, IInputProps, Input as UIInput} from 'native-base';

import {Icon} from '@components';
import {IconName, InputType, InputVariant} from '@common';
import {InputProps, Colors} from '@styles';

export type InputProps = {
  type: InputType;
  value: string;
  buttonIconName?: IconName;
  isInvalid?: boolean;
  isButtonDisabled?: boolean;
  readonly?: boolean;
  variant?: InputVariant;
  props?: IInputProps;
  onChangeText?: (text: string) => void;
  onButtonPress?: () => void;
  onBlur?: () => void;
};

export const Input: FC<InputProps> = ({
  type,
  value,
  variant = InputVariant.FILLED,
  readonly = false,
  isInvalid = false,
  isButtonDisabled = false,
  buttonIconName,
  onBlur,
  onChangeText,
  onButtonPress,
}) => {
  const {placeholder, iconName} = InputProps[type];

  return (
    <UIInput
      placeholder={placeholder}
      variant={variant}
      value={value}
      isInvalid={isInvalid}
      isReadOnly={readonly}
      onChangeText={onChangeText}
      onEndEditing={onBlur}
      _focus={{borderColor: Colors.TEAl}}
      backgroundColor={
        variant === InputVariant.OUTLINE ? Colors.WHITE : undefined
      }
      InputLeftElement={
        iconName ? <Icon name={iconName} props={{left: 4, mr: 4}} /> : undefined
      }
      InputRightElement={
        buttonIconName ? (
          <Button
            variant="unstyled"
            disabled={isButtonDisabled}
            onTouchEnd={onButtonPress}>
            <Icon name={buttonIconName} color={Colors.TEAl_DARK} />
          </Button>
        ) : undefined
      }
    />
  );
};

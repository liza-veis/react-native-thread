import React, {FC} from 'react';
import {HStack, Link as UILink, Text} from 'native-base';

type MessageProps = {
  text: string;
  buttonText: string;
  pt?: number;
  onPress: () => void;
};

export const Message: FC<MessageProps> = ({text, buttonText, pt, onPress}) => (
  <HStack justifyContent="center" pt={pt}>
    <Text>{text}</Text>
    <UILink _text={{color: 'teal.600', bold: true}} onPress={onPress}>
      {buttonText}
    </UILink>
  </HStack>
);

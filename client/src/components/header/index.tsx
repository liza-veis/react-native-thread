import React, {FC} from 'react';
import {Heading, HStack} from 'native-base';

import {Colors, FontSize} from '@styles';

type HeaderProps = {
  title: string;
};

export const Header: FC<HeaderProps> = ({title, children}) => (
  <HStack
    backgroundColor={Colors.WHITE}
    justifyContent="space-between"
    alignItems="center"
    width="100%"
    minH={55}
    shadow={2}
    pl={4}
    pr={4}>
    <Heading color={Colors.TEAl} fontSize={FontSize.LG}>
      {title}
    </Heading>
    {children}
  </HStack>
);

import React, {FC} from 'react';
import {IconButton as UIIconButton} from 'native-base';

import {IconName, IconSize} from '@common';
import {Icon} from '@components';

type IconButtonProps = {
  iconName: IconName;
  label?: string;
  onPress?: () => void;
};

export const IconButton: FC<IconButtonProps> = ({label, iconName, onPress}) => (
  <UIIconButton
    icon={<Icon label={label} name={iconName} size={IconSize.XS} />}
    onPress={onPress}
    variant="unstyled"
  />
);

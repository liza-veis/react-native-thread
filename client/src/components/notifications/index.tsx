import React, {FC, useEffect} from 'react';
import FlashMessage from 'react-native-flash-message';
import {io} from 'socket.io-client';

import {API_BASE} from '@env';
import {useSelector} from 'react-redux';
import {selectCurrentUser} from '@store/profile/selectors';
import {showInfoMessage} from '@helpers';

const socket = io(API_BASE);

export const Notifications: FC = () => {
  const user = useSelector(selectCurrentUser);

  useEffect(() => {
    if (!user) {
      return;
    }

    const {id} = user;

    socket.emit('createRoom', id);

    socket.on('like', () => {
      showInfoMessage('Your post was liked!');
    });

    return () => {
      socket.close();
    };
  }, [user]);

  return <FlashMessage position="top" />;
};

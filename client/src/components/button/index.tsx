import React, {FC} from 'react';
import {Button as UIButton, IButtonProps} from 'native-base';

import {Colors} from '@styles';
import {ButtonVariant} from '@common';

type ButtonProps = {
  variant?: ButtonVariant;
  disabled?: boolean;
  isLoading?: boolean;
  alignSelf?: IButtonProps['alignSelf'];
  minW?: number;
  p?: number;
  onPress: () => void;
};

export const Button: FC<ButtonProps> = ({
  variant = ButtonVariant.SOLID,
  disabled,
  isLoading,
  alignSelf,
  minW = 100,
  p,
  onPress,
  children,
}) => {
  const handlePress = () => {
    if (!isLoading) {
      onPress();
    }
  };

  return (
    <UIButton
      variant={variant}
      disabled={disabled}
      isLoading={isLoading}
      colorScheme="teal"
      alignSelf={alignSelf}
      backgroundColor={
        variant === ButtonVariant.SOLID ? Colors.TEAl_DARK : 'transparent'
      }
      borderColor={Colors.TEAl_DARK}
      _text={{
        fontWeight: 'bold',
        color:
          variant === ButtonVariant.SOLID ? Colors.WHITE : Colors.TEAl_DARK,
      }}
      minW={minW}
      p={p}
      onPress={handlePress}>
      {children}
    </UIButton>
  );
};

import React, {FC} from 'react';
import {HStack, Switch as UISwitch, Text} from 'native-base';

import {Colors} from '@styles';

type SwitchProps = {
  value: boolean;
  label: string;
  onChange: () => void;
};

export const Switch: FC<SwitchProps> = ({value, label, onChange}) => (
  <HStack alignItems="center" ml="auto">
    <UISwitch
      isChecked={value}
      onChange={onChange}
      onTrackColor={Colors.TEAl_DARK}
      onThumbColor={Colors.TEAl}
    />
    <Text>{label}</Text>
  </HStack>
);

import React, {FC} from 'react';
import {Share} from 'react-native';
import {AspectRatio, Box, Divider, HStack, Image, Text} from 'native-base';

import {getTimeFromNow, showErrorMessage} from '@helpers';
import {IconButton} from '@components';
import {IconName} from '@common';
import {Colors, FontSize} from '@styles';

type PostProps = {
  post: PostData;
  onLike: (id: string) => void;
  onOpen?: (post: PostData) => void;
};

export const Post: FC<PostProps> = ({post, onLike, onOpen}) => {
  const {
    id,
    body,
    image,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    user,
  } = post;
  const time = getTimeFromNow(createdAt);

  const handleLike = () => onLike(id);

  const handleOpen = () => {
    if (onOpen) {
      onOpen(post);
    }
  };

  const handleShare = () => {
    Share.share({
      message: `${body}\n${image ? image.link : ''}`,
      url: image?.link || '',
    }).catch(() => showErrorMessage());
  };

  return (
    <Box backgroundColor={Colors.WHITE} rounded="md" shadow={2} mt={3}>
      <HStack space={1} pt={3} pb={3} pl={4} pr={4}>
        <Text color={Colors.GRAY} fontSize={FontSize.SM}>
          posted by
        </Text>
        <Text color={Colors.TEAl_DARK} fontSize={FontSize.SM} fontWeight="bold">
          {user.username}
        </Text>
        <Text color={Colors.GRAY} fontSize={FontSize.SM}>
          {time}
        </Text>
      </HStack>
      {image?.link && (
        <AspectRatio w="100%" ratio={1.6} mb={2}>
          <Image source={{uri: image.link}} alt="Post image" />
        </AspectRatio>
      )}
      <Text pb={2} pl={4} pr={4}>
        {body}
      </Text>
      <Divider backgroundColor={Colors.GRAY_LIGHT} />
      <HStack space={1} pt={1} pb={1} pl={4} pr={4}>
        <IconButton
          onPress={handleLike}
          iconName={IconName.THUMBS_UP}
          label={likeCount}
        />
        <IconButton iconName={IconName.THUMBS_DOWN} label={dislikeCount} />
        <IconButton
          onPress={handleOpen}
          iconName={IconName.COMMENT}
          label={commentCount}
        />
        <Box flex={1} />
        <IconButton onPress={handleShare} iconName={IconName.SHARE} />
      </HStack>
    </Box>
  );
};

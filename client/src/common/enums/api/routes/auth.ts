export enum AuthRoutes {
  LOGIN = '/auth/login',
  REGISTER = '/auth/register',
  CURRENT_USER = '/auth/user',
}

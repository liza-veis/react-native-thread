export enum InputVariant {
  FILLED = 'filled',
  OUTLINE = 'outline',
}

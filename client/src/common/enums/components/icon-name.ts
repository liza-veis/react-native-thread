export enum IconName {
  SEND = 'send',
  USER = 'user',
  EMAIL = 'at',
  LOCK = 'lock',
  SHARE = 'share-alt',
  THUMBS_UP = 'thumbs-up',
  THUMBS_DOWN = 'thumbs-down',
  COMMENT = 'comment',
  PLUS = 'plus',
  INBOX = 'inbox',
  NONE = '',
}

export enum ButtonVariant {
  SOLID = 'solid',
  OUTLINE = 'outline',
}

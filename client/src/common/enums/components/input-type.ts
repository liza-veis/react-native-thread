export enum InputType {
  EMAIL = 'email',
  PASSWORD = 'password',
  USERNAME = 'username',
  TEXT = 'text',
}

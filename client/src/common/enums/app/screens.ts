export enum Screens {
  SIGN_IN = 'SignIn',
  SIGN_UP = 'SignUp',
  THREAD = 'Thread',
  ALL_POSTS = 'All posts',
  POST = 'Post',
  PROFILE = 'Profile',
  ADD_POST = 'AddPost',
}

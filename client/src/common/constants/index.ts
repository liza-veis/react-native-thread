export const LOGO_URL =
  'http://s1.iconbird.com/ico/2013/8/428/w256h2561377930292cattied.png';

export const DEFAULT_USER_AVATAR =
  'https://forwardsummit.ca/wp-content/uploads/2019/01/avatar-default.png';

export const LOAD_POSTS_COUNT = 10;

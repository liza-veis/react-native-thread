import 'react-native-gesture-handler';
import React, {FC} from 'react';
import {Provider as StoreProvider} from 'react-redux';
import {NativeBaseProvider as UIProvider} from 'native-base';

import {RootNavigator} from '@navigations';
import {store} from '@store';
import {Notifications} from '@components';

export const App: FC = () => (
  <StoreProvider store={store}>
    <UIProvider>
      <RootNavigator />
      <Notifications />
    </UIProvider>
  </StoreProvider>
);

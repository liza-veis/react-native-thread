import {DocumentPickerResponse} from 'react-native-document-picker';

import {HttpService} from '@services/http';
import {HttpMethod, ImageRoutes} from '@common';

type ImageServiceProps = {
  http: HttpService;
};

export class ImageService {
  private httpService: HttpService;

  constructor({http}: ImageServiceProps) {
    this.httpService = http;
  }

  uploadImage(image: DocumentPickerResponse) {
    const formData = new FormData();

    const imageData = {
      uri: image.uri,
      type: image.type,
      name: image.name,
    };

    formData.append('image', imageData);

    return this.httpService.load<ImageData>(ImageRoutes.ROOT, {
      method: HttpMethod.POST,
      payload: formData,
    });
  }
}

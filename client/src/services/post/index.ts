import {HttpService} from '@services/http';
import {ContentType, HttpMethod, PostRoutes, LOAD_POSTS_COUNT} from '@common';

type PostServiceProps = {
  http: HttpService;
};

export class PostService {
  private httpService: HttpService;

  constructor({http}: PostServiceProps) {
    this.httpService = http;
  }

  getAllPosts(filter: PostFilter) {
    const query = {...filter, count: LOAD_POSTS_COUNT};
    return this.httpService.load<PostData[]>(PostRoutes.ROOT, {
      method: HttpMethod.GET,
      query,
    });
  }

  getPost(postId: string) {
    return this.httpService.load<OpenedPostData>(
      `${PostRoutes.ROOT}/${postId}`,
      {
        method: HttpMethod.GET,
      },
    );
  }

  addPost(payload: AddPostPayload) {
    return this.httpService.load<Omit<PostData, 'user'>>(PostRoutes.ROOT, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }

  likePost(postId: string) {
    return this.httpService.load<PostReactionData>(PostRoutes.REACT, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true,
      }),
    });
  }
}

import {HttpService} from '@services/http';
import {HttpMethod, CommentRoutes, ContentType} from '@common';

type CommentServiceProps = {
  http: HttpService;
};

export class CommentService {
  private httpService: HttpService;

  constructor({http}: CommentServiceProps) {
    this.httpService = http;
  }

  getComment(id: string) {
    return this.httpService.load<CommentData>(`${CommentRoutes.ROOT}/${id}`, {
      method: HttpMethod.GET,
    });
  }

  addComment(payload: AddPostPayload) {
    return this.httpService.load<CommentCreationData>(CommentRoutes.ROOT, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload),
    });
  }
}

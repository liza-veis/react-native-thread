import AsyncStorage from '@react-native-async-storage/async-storage';

export class StorageService {
  private storage = AsyncStorage;

  getItem(key: string) {
    return this.storage.getItem(key);
  }

  setItem(key: string, value: string) {
    this.storage.setItem(key, value);
  }

  removeItem(key: string) {
    this.storage.removeItem(key);
  }

  clear() {
    this.storage.clear();
  }
}

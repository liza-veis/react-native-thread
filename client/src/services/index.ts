import {StorageService} from '@services/storage';
import {HttpService} from '@services/http';
import {AuthService} from '@services/auth';
import {ImageService} from '@services/image';
import {CommentService} from '@services/comment';
import {PostService} from '@services/post';

export const storageService = new StorageService();

export const httpService = new HttpService({
  storage: storageService,
});

export const authService = new AuthService({
  http: httpService,
});

export const imageService = new ImageService({
  http: httpService,
});

export const commentService = new CommentService({
  http: httpService,
});

export const postService = new PostService({
  http: httpService,
});

import {HttpService} from '@services/http';
import {ContentType, HttpMethod, AuthRoutes} from '@common';

type AuthServiceProps = {
  http: HttpService;
};

export class AuthService {
  private httpService: HttpService;

  constructor({http}: AuthServiceProps) {
    this.httpService = http;
  }

  login(payload: LoginPayload) {
    return this.httpService.load<AuthReturnData>(AuthRoutes.LOGIN, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload),
    });
  }

  register(payload: RegistrationPayload) {
    return this.httpService.load<AuthReturnData>(AuthRoutes.REGISTER, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      hasAuth: false,
      payload: JSON.stringify(payload),
    });
  }

  getCurrentUser() {
    return this.httpService.load<UserData>(AuthRoutes.CURRENT_USER, {
      method: HttpMethod.GET,
    });
  }
}

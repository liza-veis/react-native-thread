import {API_BASE, API_PATH} from '@env';
import {StorageService} from '@services/storage';
import {ContentType, HttpHeader, HttpMethod, StorageKey} from '@common';
import {stringifyQuery} from '@helpers';

const API_URL = `${API_BASE}${API_PATH}`;

type HttpServiceProps = {
  storage: StorageService;
};

type LoadOptions = {
  method?: HttpMethod;
  payload?: FormData | string;
  hasAuth?: boolean;
  contentType?: ContentType;
  query?: Query;
};

type GetHeadersOptions = {
  hasAuth?: boolean;
  contentType?: ContentType;
};

export class HttpService {
  private storageService: StorageService;

  constructor({storage}: HttpServiceProps) {
    this.storageService = storage;
  }

  async load<T>(url: string, options: LoadOptions = {}): Promise<T> {
    const {
      method = HttpMethod.GET,
      payload,
      hasAuth = true,
      contentType,
      query,
    } = options;

    const headers = await this.getHeaders({
      hasAuth,
      contentType,
    });

    return fetch(this.getUrl(url, query), {
      method,
      headers,
      body: payload,
    })
      .then(this.checkStatus)
      .then(this.parseJSON);
  }

  private async getHeaders({hasAuth, contentType}: GetHeadersOptions) {
    const headers = {};

    if (contentType) {
      Object.assign(headers, {[HttpHeader.CONTENT_TYPE]: contentType});
    }

    if (hasAuth) {
      const token = await this.storageService.getItem(StorageKey.TOKEN);

      Object.assign(headers, {[HttpHeader.AUTHORIZATION]: `Bearer ${token}`});
    }

    return headers;
  }

  private async checkStatus(response: Response) {
    if (!response.ok) {
      const parsedException = await response.json().catch(() => ({
        message: response.statusText,
      }));

      throw new Error(parsedException?.message);
    }

    return response;
  }

  private parseJSON(response: Response) {
    return response.json();
  }

  private getUrl(url: string, query?: Query) {
    return `${API_URL}${url}${query ? `?${stringifyQuery(query)}` : ''}`;
  }
}

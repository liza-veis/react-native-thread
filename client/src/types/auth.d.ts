type LoginPayload = {
  email: string;
  password: string;
};

type RegistrationPayload = {
  username: string;
  email: string;
  password: string;
};

type AuthReturnData = {
  user: UserData;
  token: string;
};

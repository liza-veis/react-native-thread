type ImageData = {
  id: string;
  link: string;
};

type UserData = {
  id: string;
  email: string;
  image: ImageData | null;
  username: string;
  createdAt: string;
  updatedAt: string;
};

type PostData = {
  id: string;
  body: string;
  image: ImageData | null;
  likeCount: string;
  dislikeCount: string;
  commentCount: string;
  createdAt: string;
  updatedAt: string;
  user: Pick<UserData, 'id' | 'image' | 'username'>;
  userId: string;
};

type CommentData = {
  id: string;
  body: string;
  createdAt: string;
  updatedAt: string;
  user: Pick<UserData, 'id' | 'image' | 'username'>;
  userId: string;
  postId: string;
};

type CommentCreationData = Omit<CommentData, 'user'>;

type OpenedPostData = PostData & {comments: CommentData[]};

type PostReactionData = {
  id: string;
  createdAt: string;
  isLike: boolean;
  post: Pick<PostData, 'id' | 'userId'>;
  updatedAt: string;
  postId: string;
  userId: string;
};

type PostFilter = {
  userId?: string;
  from: number;
};

type AddPostPayload = {
  body: string;
  imageId?: string;
};

type AddCommentPayload = {
  postId: string;
  body: string;
};

type ProfileState = {
  user: UserData | null;
  isError: boolean;
  isLoading: boolean;
};

type ThreadState = {
  posts: PostData[];
  openedPost: OpenedPostData | null;
  isLoading: boolean;
  hasMorePosts: boolean;
};

type RootState = {
  profile: ProfileState;
  thread: ThreadState;
};

type Query = {
  [key: string]: string | number | undefined;
};

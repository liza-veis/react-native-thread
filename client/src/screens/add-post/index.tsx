import React, {FC, useState} from 'react';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {Box, TextArea, VStack, Image, AspectRatio} from 'native-base';

import {Header, Button} from '@components';
import * as threadActions from '@store/thread/actions';
import {imageService} from '@services';
import {pickImage, showErrorMessage} from '@helpers';
import {ButtonVariant, Screens} from '@common';
import {Colors} from '@styles';

export const AddPostScreen: FC = () => {
  const navigator = useNavigation();
  const dispatch = useDispatch();

  const [body, setBody] = useState('');
  const [image, setImage] = useState<ImageData | null>(null);
  const [isUploading, setIsUploading] = useState(false);

  const handleImageUpload = () => {
    setIsUploading(true);
    pickImage()
      .then(data => imageService.uploadImage(data))
      .then(({id, link}) => setImage({id, link}))
      .catch(() => showErrorMessage())
      .finally(() => setIsUploading(false));
  };

  const handleSubmit = () => {
    dispatch(threadActions.setOpenedPost(null));
    dispatch(threadActions.addPost({imageId: image?.id, body}));
    navigator.navigate(Screens.ALL_POSTS);
    setBody('');
    setImage(null);
  };

  return (
    <Box>
      <Header title="Add a post">
        <Button
          onPress={handleSubmit}
          disabled={isUploading || !body.trim()}
          p={2.5}>
          Post
        </Button>
      </Header>
      <VStack p={4} space={4}>
        <TextArea
          value={body}
          onChangeText={setBody}
          textAlignVertical="top"
          placeholder="Write something..."
          backgroundColor={Colors.WHITE}
          _focus={{borderColor: Colors.TEAl}}
          h={40}
        />
        <Button
          variant={ButtonVariant.OUTLINE}
          isLoading={isUploading}
          alignSelf="flex-start"
          minW={150}
          onPress={handleImageUpload}>
          Attach image
        </Button>
        {image?.link && (
          <AspectRatio w="100%" ratio={1.6} mb={2}>
            <Image source={{uri: image.link}} alt="Post image" />
          </AspectRatio>
        )}
      </VStack>
    </Box>
  );
};

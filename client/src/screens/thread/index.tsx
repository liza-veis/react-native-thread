import React, {FC, useEffect, useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Box, FlatList} from 'native-base';
import {useIsFocused, useNavigation} from '@react-navigation/native';

import {Post, Header, Switch, Loader} from '@components';
import {
  selectPosts,
  selectHasMorePosts,
  selectPostsLoading,
} from '@store/thread/selectors';
import {selectCurrentUser} from '@store/profile/selectors';
import * as threadActions from '@store/thread/actions';
import {LOAD_POSTS_COUNT, Screens} from '@common';

const filter: PostFilter = {
  userId: undefined,
  from: 0,
};

export const ThreadScreen: FC = () => {
  const navigator = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const user = useSelector(selectCurrentUser);
  const posts = useSelector(selectPosts);
  const isLoading = useSelector(selectPostsLoading);
  const hasMorePosts = useSelector(selectHasMorePosts);

  const {id: userId} = user || {};
  const showLoader = isLoading && posts.length === 0;
  const showFooterLoader = hasMorePosts && posts.length;

  const toggleOwnPosts = () => {
    filter.from = 0;
    filter.userId = filter.userId ? undefined : userId;
    dispatch(threadActions.loadAllPosts(filter));
  };

  const handleLike = useCallback(
    (id: string) => dispatch(threadActions.likePost(id)),
    [dispatch],
  );

  const handlePostOpen = useCallback(
    ({id}: PostData) => {
      navigator.navigate(Screens.POST);
      dispatch(threadActions.loadOpenedPost(id));
      filter.userId = undefined;
    },
    [dispatch, navigator],
  );

  const loadMorePosts = () => {
    if (hasMorePosts && !isLoading) {
      filter.from += LOAD_POSTS_COUNT;
      dispatch(threadActions.loadMorePosts(filter));
    }
  };

  const renderPost = ({item}: {item: PostData}) => (
    <Post post={item} onLike={handleLike} onOpen={handlePostOpen} />
  );

  useEffect(() => {
    if (!isFocused) {
      filter.userId = undefined;
      dispatch(threadActions.loadAllPosts(filter));
    }
  }, [dispatch, isFocused]);

  useEffect(() => {
    dispatch(threadActions.loadAllPosts(filter));
  }, [dispatch]);

  return (
    <Box flex={1} pb={5}>
      <Header title="Thread">
        <Switch
          label="Show my posts"
          value={Boolean(filter.userId)}
          onChange={toggleOwnPosts}
        />
      </Header>
      <Box pl={3} pr={3} flex={1}>
        {showLoader && <Loader fullHeight />}
        <FlatList
          data={posts}
          renderItem={renderPost}
          keyExtractor={({id}) => id}
          onEndReachedThreshold={0.5}
          onEndReached={loadMorePosts}
          initialNumToRender={LOAD_POSTS_COUNT}
          ListFooterComponent={showFooterLoader ? <Loader /> : null}
        />
      </Box>
    </Box>
  );
};

import React, {FC, useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {Box} from 'native-base';

import {InputType, Screens} from '@common';
import {AuthForm, Input, Logo, Message} from '@components';
import * as profileActions from '@store/profile/actions';
import {selectAuthError, selectAuthLoading} from '@store/profile/selectors';
import {showErrorMessage, validate} from '@helpers';

const initialData = {
  username: '',
  email: '',
  password: '',
};

const initialValidData = {
  username: true,
  email: true,
  password: true,
};

export const SignUpScreen: FC = () => {
  const navigator = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const isAuthError = useSelector(selectAuthError);
  const isLoading = useSelector(selectAuthLoading);

  const [data, setData] = useState(initialData);
  const [validData, setValidData] = useState(initialValidData);

  const validateUsername = () => {
    if (isFocused) {
      setValidData({
        ...validData,
        username: validate(data.username, InputType.USERNAME),
      });
    }
  };

  const validateEmail = () => {
    if (isFocused) {
      setValidData({
        ...validData,
        email: validate(data.email, InputType.EMAIL),
      });
    }
  };

  const validatePassword = () => {
    if (isFocused) {
      setValidData({
        ...validData,
        password: validate(data.password, InputType.PASSWORD),
      });
    }
  };

  const isAllValid = () => {
    if (isAuthError) {
      return false;
    }

    return (
      validate(data.email, InputType.EMAIL) &&
      validate(data.password, InputType.PASSWORD) &&
      validate(data.username, InputType.USERNAME)
    );
  };

  const handleUsernameChange = (text: string) => {
    setData({...data, username: text});
    setValidData({...validData, username: true});
  };

  const handleEmailChange = (text: string) => {
    setData({...data, email: text});
    setValidData({...validData, email: true});
  };

  const handlePasswordChange = (text: string) => {
    setData({...data, password: text});
    setValidData({...validData, password: true});
  };

  const handleSignInPress = () => {
    navigator.navigate(Screens.SIGN_IN);
    setData(initialData);
    setValidData(initialValidData);
  };

  const handleSubmit = () => {
    if (isAllValid()) {
      dispatch(profileActions.register(data));
    } else {
      validateEmail();
      validateUsername();
      validatePassword();
    }
  };

  useEffect(() => {
    if (isAuthError) {
      dispatch(profileActions.setError(false));
      showErrorMessage('Something went wrong');
    }
  }, [dispatch, isAuthError]);

  return (
    <Box flexGrow={1} justifyContent="center" alignItems="center">
      <Logo />
      <AuthForm
        heading="Create a new account"
        buttonName="Register"
        isLoading={isLoading}
        onSubmit={handleSubmit}>
        <Input
          type={InputType.USERNAME}
          value={data.username}
          onChangeText={handleUsernameChange}
          isInvalid={!validData.username || isAuthError}
          onBlur={validateUsername}
        />
        <Input
          type={InputType.EMAIL}
          value={data.email}
          onChangeText={handleEmailChange}
          isInvalid={!validData.email || isAuthError}
          onBlur={validateEmail}
        />
        <Input
          type={InputType.PASSWORD}
          value={data.password}
          onChangeText={handlePasswordChange}
          isInvalid={!validData.password || isAuthError}
          onBlur={validatePassword}
        />
      </AuthForm>
      <Message
        text="Already with us? "
        buttonText="Sign in"
        onPress={handleSignInPress}
        pt={5}
      />
    </Box>
  );
};

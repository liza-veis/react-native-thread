import React, {FC, useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {Box} from 'native-base';

import {InputType, Screens} from '@common';
import {showErrorMessage, validate} from '@helpers';
import {AuthForm, Input, Logo, Message} from '@components';
import * as profileActions from '@store/profile/actions';
import {selectAuthError, selectAuthLoading} from '@store/profile/selectors';

const initialData = {
  email: '',
  password: '',
};

const initialValidData = {
  email: true,
  password: true,
};

export const SignInScreen: FC = () => {
  const navigator = useNavigation();
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const isAuthError = useSelector(selectAuthError);
  const isLoading = useSelector(selectAuthLoading);

  const [data, setData] = useState(initialData);
  const [validData, setValidData] = useState(initialValidData);

  const validateEmail = () => {
    if (isFocused) {
      setValidData({
        ...validData,
        email: validate(data.email, InputType.EMAIL),
      });
    }
  };

  const validatePassword = () => {
    if (isFocused) {
      setValidData({
        ...validData,
        password: validate(data.password, InputType.PASSWORD),
      });
    }
  };

  const handleEmailChange = (text: string) => {
    setData({...data, email: text});
    setValidData({
      ...validData,
      email: true,
    });
  };

  const isAllValid = () => {
    if (isAuthError) {
      return false;
    }

    return (
      validate(data.email, InputType.EMAIL) &&
      validate(data.password, InputType.PASSWORD)
    );
  };

  const handlePasswordChange = (text: string) => {
    setData({...data, password: text});
    setValidData({
      ...validData,
      password: true,
    });
  };

  const handleSignUpPress = () => {
    navigator.navigate(Screens.SIGN_UP);
    setData(initialData);
    setValidData(initialValidData);
  };

  const handleSubmit = () => {
    if (isAllValid()) {
      dispatch(profileActions.login(data));
    } else {
      validateEmail();
      validatePassword();
    }
  };

  useEffect(() => {
    if (isAuthError) {
      dispatch(profileActions.setError(false));
      showErrorMessage('Invalid email or password');
    }
  }, [dispatch, isAuthError]);

  return (
    <Box flexGrow={1} justifyContent="center" alignItems="center">
      <Logo />
      <AuthForm
        heading="Login to your account"
        buttonName="Login"
        isLoading={isLoading}
        onSubmit={handleSubmit}>
        <Input
          type={InputType.EMAIL}
          value={data.email}
          readonly={isLoading}
          isInvalid={!validData.email}
          onChangeText={handleEmailChange}
          onBlur={validateEmail}
        />
        <Input
          type={InputType.PASSWORD}
          value={data.password}
          readonly={isLoading}
          isInvalid={!validData.password}
          onChangeText={handlePasswordChange}
          onBlur={validatePassword}
        />
      </AuthForm>
      <Message
        text="New to us? "
        buttonText="Sign Up"
        onPress={handleSignUpPress}
        pt={5}
      />
    </Box>
  );
};

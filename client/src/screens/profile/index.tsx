import React, {FC} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Image, Box, VStack} from 'native-base';

import {
  ButtonVariant,
  DEFAULT_USER_AVATAR,
  InputType,
  InputVariant,
} from '@common';
import {selectCurrentUser} from '@store/profile/selectors';
import * as profileActions from '@store/profile/actions';
import {Input, Header, Button} from '@components';

export const ProfileScreen: FC = () => {
  const dispatch = useDispatch();
  const user = useSelector(selectCurrentUser);

  const {username = '', email = '', image} = user || {};

  const handleLogout = () => {
    dispatch(profileActions.logout());
  };

  return (
    <Box alignItems="center">
      <Header title="Profile">
        <Button
          variant={ButtonVariant.OUTLINE}
          minW={100}
          p={2}
          onPress={handleLogout}>
          Logout
        </Button>
      </Header>
      <VStack p={5} mt={5} space={3} maxW={300}>
        <Image
          size={250}
          rounded={125}
          source={{uri: image?.link || DEFAULT_USER_AVATAR}}
          alt="User avatar"
          mb={2}
        />
        <Input
          variant={InputVariant.OUTLINE}
          type={InputType.USERNAME}
          readonly
          value={username}
        />
        <Input
          variant={InputVariant.OUTLINE}
          type={InputType.EMAIL}
          readonly
          value={email}
        />
      </VStack>
    </Box>
  );
};

import React, {FC, useState, useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {VStack, ScrollView, Heading, Divider, Text} from 'native-base';

import {Comment, Input, Loader, Post} from '@components';
import {selectOpenedPost} from '@store/thread/selectors';
import * as threadActions from '@store/thread/actions';
import {sortComments} from '@helpers';
import {IconName, InputType, InputVariant} from '@common';
import {Colors, FontSize} from '@styles';

export const PostScreen: FC = () => {
  const dispatch = useDispatch();
  const post = useSelector(selectOpenedPost);

  const [commentBody, setCommentBody] = useState<string>('');

  const comments = sortComments(post?.comments ?? []);

  const handleLike = useCallback(
    (id: string) => dispatch(threadActions.likePost(id)),
    [dispatch],
  );

  const handleAddComment = useCallback(() => {
    if (post && commentBody.trim()) {
      dispatch(threadActions.addComment({body: commentBody, postId: post.id}));
      setCommentBody('');
    }
  }, [dispatch, commentBody, post]);

  if (!post) {
    return <Loader fullHeight />;
  }

  return (
    <ScrollView flex={1}>
      <Post post={post} onLike={handleLike} />
      <VStack mt={4} p={4} backgroundColor={Colors.WHITE}>
        <Heading fontSize={FontSize.LG} color={Colors.GRAY} mb={4}>
          Comments
        </Heading>
        <Input
          variant={InputVariant.OUTLINE}
          type={InputType.TEXT}
          value={commentBody}
          buttonIconName={IconName.SEND}
          onChangeText={setCommentBody}
          onButtonPress={handleAddComment}
          isButtonDisabled={!commentBody.trim()}
        />
        <Divider mt={5} mb={5} />
        <VStack space={4}>
          {comments.length ? (
            comments.map(comment => (
              <Comment key={comment.id} comment={comment} />
            ))
          ) : (
            <Text
              alignSelf="center"
              color={Colors.GRAY}
              fontSize={FontSize.MD}
              lineHeight={4}>
              No comments yet
            </Text>
          )}
        </VStack>
      </VStack>
    </ScrollView>
  );
};

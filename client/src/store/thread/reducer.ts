import {createReducer} from '@reduxjs/toolkit';

import {LOAD_POSTS_COUNT} from '@common';
import {
  loadAllPosts,
  loadMorePosts,
  setAllPosts,
  setOpenedPost,
} from './actions';

const initialState: ThreadState = {
  posts: [],
  openedPost: null,
  isLoading: false,
  hasMorePosts: false,
};

export const threadReducer = createReducer(initialState, builder => {
  builder.addCase(setAllPosts, (state, action) => {
    const {posts} = action.payload;

    if (posts.length) {
      const newPostsCount = posts.length - state.posts.length;
      state.hasMorePosts = newPostsCount >= LOAD_POSTS_COUNT;
    }
    state.isLoading = false;
    state.posts = posts;
  });

  builder.addCase(setOpenedPost, (state, action) => {
    const {post} = action.payload;
    state.openedPost = post;
  });

  builder.addCase(loadAllPosts.pending, state => {
    state.isLoading = true;
    state.posts = [];
  });

  builder.addCase(loadMorePosts.pending, state => {
    state.isLoading = true;
  });
});

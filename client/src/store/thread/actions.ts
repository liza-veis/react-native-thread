import {createAction, createAsyncThunk} from '@reduxjs/toolkit';

import {mapPostCommentCount, mapPostComments, mapPostLikes} from '@helpers';
import {commentService, postService} from '@services';
import {selectOpenedPost, selectPosts} from './selectors';

const ActionType = {
  SET_ALL_POSTS: 'profile/set-all-posts',
  SET_OPENED_POST: 'profile/set-opened-post',
  ADD_POST: 'profile/add-post',
  LOAD_ALL_POSTS: 'profile/load-all-post',
  LOAD_MORE_POSTS: 'profile/load-more-post',
  LIKE_POST: 'profile/like-post',
  LOAD_OPENED_POST: 'profile/load-opened-post',
  ADD_COMMENT: 'profile/add-comment',
};

export const setAllPosts = createAction(
  ActionType.SET_ALL_POSTS,
  (posts: PostData[]) => ({
    payload: {posts},
  }),
);

export const setOpenedPost = createAction(
  ActionType.SET_OPENED_POST,
  (post: OpenedPostData | null) => ({
    payload: {post},
  }),
);

export const loadAllPosts = createAsyncThunk(
  ActionType.LOAD_ALL_POSTS,
  async (filter: PostFilter, thunkApi) => {
    const posts = await postService.getAllPosts(filter);
    thunkApi.dispatch(setAllPosts(posts));
  },
);

export const loadMorePosts = createAsyncThunk(
  ActionType.LOAD_MORE_POSTS,
  async (filter: PostFilter, thunkApi) => {
    const state = thunkApi.getState() as RootState;
    const posts = selectPosts(state);
    const newPosts = await postService.getAllPosts(filter);

    thunkApi.dispatch(setAllPosts(posts.concat(newPosts)));
  },
);

export const loadOpenedPost = createAsyncThunk(
  ActionType.LOAD_OPENED_POST,
  async (postId: string, thunkApi) => {
    const post = await postService.getPost(postId);
    thunkApi.dispatch(setOpenedPost(post));
  },
);

export const addPost = createAsyncThunk(
  ActionType.ADD_POST,
  async (payload: AddPostPayload, thunkApi) => {
    await postService.addPost(payload);
    thunkApi.dispatch(loadAllPosts({from: 0}));
  },
);

export const likePost = createAsyncThunk(
  ActionType.LIKE_POST,
  async (postId: string, thunkApi) => {
    const state = thunkApi.getState() as RootState;
    const posts = selectPosts(state);
    const openedPost = selectOpenedPost(state);

    const {id} = await postService.likePost(postId);
    const diff = id ? 1 : -1;

    const updatedPosts = posts.map(post =>
      post.id === postId ? mapPostLikes(post, diff) : post,
    );

    thunkApi.dispatch(setAllPosts(updatedPosts));

    if (openedPost && openedPost?.id === postId) {
      thunkApi.dispatch(setOpenedPost(mapPostLikes(openedPost, diff)));
    }
  },
);

export const addComment = createAsyncThunk(
  ActionType.ADD_COMMENT,
  async (payload: AddCommentPayload, thunkApi) => {
    const {id} = await commentService.addComment(payload);
    const comment = await commentService.getComment(id);
    const state = thunkApi.getState() as RootState;
    const posts = selectPosts(state);
    const openedPost = selectOpenedPost(state);

    const updatedPosts = posts.map(post =>
      post.id === payload.postId ? mapPostCommentCount(post) : post,
    );

    thunkApi.dispatch(setAllPosts(updatedPosts));

    if (openedPost?.id === payload.postId) {
      thunkApi.dispatch(setOpenedPost(mapPostComments(openedPost, comment)));
    }
  },
);

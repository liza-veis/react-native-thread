export const selectPosts = (state: RootState) => state.thread.posts;

export const selectOpenedPost = (state: RootState) => state.thread.openedPost;

export const selectPostsLoading = (state: RootState) => state.thread.isLoading;

export const selectHasMorePosts = (state: RootState) =>
  state.thread.hasMorePosts;

import {createAction, createAsyncThunk} from '@reduxjs/toolkit';

import {StorageKey} from '@common';
import {storageService, authService} from '@services';

const ActionType = {
  SET_USER: 'profile/set-user',
  SET_ERROR: 'profile/set-error',
  LOGIN: 'profile/login',
  REGISTER: 'profile/register',
  LOGOUT: 'profile/logout',
  LOAD_CURRENT_USER: 'profile/load-current-user',
};

export const setUser = createAction(
  ActionType.SET_USER,
  (user: UserData | null) => ({
    payload: {user},
  }),
);

export const setError = createAction(
  ActionType.SET_ERROR,
  (isError: boolean) => ({
    payload: {isError},
  }),
);

export const login = createAsyncThunk(
  ActionType.LOGIN,
  async (payload: LoginPayload, thunkApi) => {
    const {user, token} = await authService.login(payload);

    await storageService.setItem(StorageKey.TOKEN, token);
    thunkApi.dispatch(setUser(user));
  },
);

export const register = createAsyncThunk(
  ActionType.REGISTER,
  async (payload: RegistrationPayload, thunkApi) => {
    const {user, token} = await authService.register(payload);

    await storageService.setItem(StorageKey.TOKEN, token);
    thunkApi.dispatch(setUser(user));
  },
);

export const loadCurrentUser = createAsyncThunk(
  ActionType.LOAD_CURRENT_USER,
  async (_, thunkApi) => {
    const token = await storageService.getItem(StorageKey.TOKEN);
    if (!token) {
      return thunkApi.dispatch(setUser(null));
    }

    try {
      const user = await authService.getCurrentUser();
      thunkApi.dispatch(setUser(user));
    } catch {
      thunkApi.dispatch(logout());
    }
  },
);

export const logout = createAsyncThunk(
  ActionType.LOGOUT,
  async (_, thunkApi) => {
    await storageService.removeItem(StorageKey.TOKEN);
    thunkApi.dispatch(setUser(null));
  },
);

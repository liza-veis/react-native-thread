export const selectCurrentUser = (state: RootState) => state.profile.user;

export const selectAuthError = (state: RootState) => state.profile.isError;

export const selectAuthLoading = (state: RootState) => state.profile.isLoading;

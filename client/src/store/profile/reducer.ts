import {createReducer} from '@reduxjs/toolkit';

import {login, register, setError, setUser} from './actions';

const initialState: ProfileState = {
  user: null,
  isError: false,
  isLoading: false,
};

const setPending = (state: ProfileState) => {
  state.isError = false;
  state.isLoading = true;
};

const setRejected = (state: ProfileState) => {
  state.isError = true;
  state.isLoading = false;
};

export const profileReducer = createReducer(initialState, builder => {
  builder.addCase(setUser, (state, action) => {
    const {user} = action.payload;
    state.user = user;
    state.isError = false;
    state.isLoading = false;
  });

  builder.addCase(setError, (state, action) => {
    const {isError} = action.payload;
    state.isError = isError;
  });

  builder.addCase(login.pending, setPending);

  builder.addCase(register.pending, setPending);

  builder.addCase(login.rejected, setRejected);

  builder.addCase(register.rejected, setRejected);
});

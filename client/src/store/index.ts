import {configureStore} from '@reduxjs/toolkit';

import {profileReducer} from '@store/profile';
import {threadReducer} from '@store/thread';

export const store = configureStore<RootState>({
  reducer: {
    profile: profileReducer,
    thread: threadReducer,
  },
});

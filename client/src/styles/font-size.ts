export enum FontSize {
  XS = 12,
  SM = 14,
  MD = 16,
  LG = 24,
  XL = 30,
}

export enum Colors {
  WHITE = '#ffffff',
  BLACK = '#000000',
  TEAl = '#0d9488',
  TEAl_DARK = '#0f766e',
  GRAY = '#71717a',
  GRAY_LIGHT = '#e4e4e7',
}

import {IconName, InputType} from '@common';

export const InputProps = {
  [InputType.USERNAME]: {
    placeholder: 'username',
    iconName: IconName.USER,
  },
  [InputType.EMAIL]: {
    placeholder: 'email',
    iconName: IconName.EMAIL,
  },
  [InputType.PASSWORD]: {
    placeholder: 'password',
    iconName: IconName.LOCK,
  },
  [InputType.TEXT]: {
    placeholder: 'Write something...',
    iconName: IconName.NONE,
  },
};

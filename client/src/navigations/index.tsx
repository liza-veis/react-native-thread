import React, {FC, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';

import {AuthNavigator} from '@navigations/auth-navigator';
import {AppNavigator} from '@navigations/app-navigator';
import {selectCurrentUser} from '@store/profile/selectors';
import * as profileActions from '@store/profile/actions';

export const RootNavigator: FC = () => {
  const dispatch = useDispatch();
  const user = useSelector(selectCurrentUser);

  useEffect(() => {
    dispatch(profileActions.loadCurrentUser());
  }, [dispatch]);

  return (
    <NavigationContainer>
      {user ? <AppNavigator /> : <AuthNavigator />}
    </NavigationContainer>
  );
};

import React, {FC} from 'react';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';

import {PostScreen, ThreadScreen} from '@screens';
import {Screens} from '@common';
import {Colors} from '@styles';

const Stack = createStackNavigator();

const threadOptions = {headerShown: false};

const postOptions: StackNavigationOptions = {
  headerTintColor: Colors.TEAl,
  headerTitleStyle: {
    color: Colors.TEAl,
  },
  headerStyle: {
    minHeight: 55,
    shadowColor: Colors.BLACK,
  },
};

export const ThreadNavigator: FC = () => (
  <Stack.Navigator initialRouteName={Screens.ALL_POSTS}>
    <Stack.Screen
      name={Screens.ALL_POSTS}
      component={ThreadScreen}
      options={threadOptions}
    />
    <Stack.Screen
      name={Screens.POST}
      component={PostScreen}
      options={postOptions}
    />
  </Stack.Navigator>
);

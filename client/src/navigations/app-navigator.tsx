import React, {FC} from 'react';
import {
  BottomTabNavigationOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';

import {ThreadNavigator} from '@navigations/thread-navigator';
import {AddPostScreen, ProfileScreen} from '@screens';
import {IconName, Screens} from '@common';
import {TabBarIcon} from '@components';

const Tab = createBottomTabNavigator();

const getTabBarOptions = (iconName: IconName): BottomTabNavigationOptions => ({
  tabBarIcon: ({focused}) => (
    <TabBarIcon iconName={iconName} focused={focused} />
  ),
  tabBarLabel: () => null,
});

export const AppNavigator: FC = () => (
  <Tab.Navigator initialRouteName={Screens.THREAD}>
    <Tab.Screen
      name={Screens.PROFILE}
      component={ProfileScreen}
      options={getTabBarOptions(IconName.USER)}
    />
    <Tab.Screen
      name={Screens.THREAD}
      component={ThreadNavigator}
      options={getTabBarOptions(IconName.INBOX)}
    />
    <Tab.Screen
      name={Screens.ADD_POST}
      component={AddPostScreen}
      options={getTabBarOptions(IconName.PLUS)}
    />
  </Tab.Navigator>
);

import React, {FC} from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {SignInScreen, SignUpScreen} from '@screens';
import {Screens} from '@common';

const Stack = createStackNavigator();

const options = {headerShown: false};

export const AuthNavigator: FC = () => (
  <Stack.Navigator initialRouteName={Screens.SIGN_IN}>
    <Stack.Screen
      name={Screens.SIGN_IN}
      component={SignInScreen}
      options={options}
    />
    <Stack.Screen
      name={Screens.SIGN_UP}
      component={SignUpScreen}
      options={options}
    />
  </Stack.Navigator>
);
